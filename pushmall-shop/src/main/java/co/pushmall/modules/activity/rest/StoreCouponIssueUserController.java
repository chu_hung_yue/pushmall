package co.pushmall.modules.activity.rest;

import co.pushmall.aop.log.Log;
import co.pushmall.modules.activity.domain.PushMallStoreCouponIssueUser;
import co.pushmall.modules.activity.service.PushMallStoreCouponIssueUserService;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponIssueUserQueryCriteria;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pushmall
 * @date 2019-11-09
 */
@Api(tags = "商城:优惠券前台用户领取记录管理")
@RestController
@RequestMapping("api")
public class StoreCouponIssueUserController {

    private final PushMallStoreCouponIssueUserService pushMallStoreCouponIssueUserService;

    public StoreCouponIssueUserController(PushMallStoreCouponIssueUserService pushMallStoreCouponIssueUserService) {
        this.pushMallStoreCouponIssueUserService = pushMallStoreCouponIssueUserService;
    }

    @Log("查询")
    @ApiOperation(value = "查询")
    @GetMapping(value = "/PmStoreCouponIssueUser")
    @PreAuthorize("@el.check('admin','YXSTORECOUPONISSUEUSER_ALL','YXSTORECOUPONISSUEUSER_SELECT')")
    public ResponseEntity getPushMallStoreCouponIssueUsers(PushMallStoreCouponIssueUserQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity(pushMallStoreCouponIssueUserService.queryAll(criteria, pageable), HttpStatus.OK);
    }

    @Log("新增")
    @ApiOperation(value = "新增")
    @PostMapping(value = "/PmStoreCouponIssueUser")
    @PreAuthorize("@el.check('admin','YXSTORECOUPONISSUEUSER_ALL','YXSTORECOUPONISSUEUSER_CREATE')")
    public ResponseEntity create(@Validated @RequestBody PushMallStoreCouponIssueUser resources) {
        return new ResponseEntity(pushMallStoreCouponIssueUserService.create(resources), HttpStatus.CREATED);
    }

    @Log("修改")
    @ApiOperation(value = "修改")
    @PutMapping(value = "/PmStoreCouponIssueUser")
    @PreAuthorize("@el.check('admin','YXSTORECOUPONISSUEUSER_ALL','YXSTORECOUPONISSUEUSER_EDIT')")
    public ResponseEntity update(@Validated @RequestBody PushMallStoreCouponIssueUser resources) {
        pushMallStoreCouponIssueUserService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除")
    @ApiOperation(value = "删除")
    @DeleteMapping(value = "/PmStoreCouponIssueUser/{id}")
    @PreAuthorize("@el.check('admin','YXSTORECOUPONISSUEUSER_ALL','YXSTORECOUPONISSUEUSER_DELETE')")
    public ResponseEntity delete(@PathVariable Integer id) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        pushMallStoreCouponIssueUserService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
