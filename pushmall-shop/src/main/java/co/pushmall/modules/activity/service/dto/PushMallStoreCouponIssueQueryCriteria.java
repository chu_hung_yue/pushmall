package co.pushmall.modules.activity.service.dto;

import co.pushmall.annotation.Query;
import lombok.Data;

/**
 * @author pushmall
 * @date 2019-11-09
 */
@Data
public class PushMallStoreCouponIssueQueryCriteria {
    @Query
    private Integer isDel;
}
