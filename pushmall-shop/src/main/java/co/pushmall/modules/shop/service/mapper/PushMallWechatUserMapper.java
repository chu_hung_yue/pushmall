package co.pushmall.modules.shop.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.modules.shop.domain.PushMallWechatUser;
import co.pushmall.modules.shop.service.dto.PushMallWechatUserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-12-13
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallWechatUserMapper extends EntityMapper<PushMallWechatUserDTO, PushMallWechatUser> {

}
