package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallMaterial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2020-01-09
 */
public interface PushMallMaterialRepository extends JpaRepository<PushMallMaterial, String>, JpaSpecificationExecutor<PushMallMaterial> {
}
