package co.pushmall.modules.shop.service.impl;

import co.pushmall.modules.shop.domain.PushMallUserBill;
import co.pushmall.modules.shop.repository.PushMallUserBillRepository;
import co.pushmall.modules.shop.service.PushMallUserBillService;
import co.pushmall.modules.shop.service.dto.PushMallUserBillDTO;
import co.pushmall.modules.shop.service.dto.PushMallUserBillQueryCriteria;
import co.pushmall.modules.shop.service.mapper.PushMallUserBillMapper;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-11-06
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallUserBillServiceImpl implements PushMallUserBillService {

    private final PushMallUserBillRepository pushMallUserBillRepository;

    private final PushMallUserBillMapper pushMallUserBillMapper;

    public PushMallUserBillServiceImpl(PushMallUserBillRepository pushMallUserBillRepository, PushMallUserBillMapper pushMallUserBillMapper) {
        this.pushMallUserBillRepository = pushMallUserBillRepository;
        this.pushMallUserBillMapper = pushMallUserBillMapper;
    }

    @Override
    public Map<String, Object> queryAll(PushMallUserBillQueryCriteria criteria, Pageable pageable) {
        Page<Map> page = pushMallUserBillRepository.findAllByPageable(criteria.getCategory()
                , criteria.getType(), criteria.getNickname(), pageable);
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", page.getContent());
        map.put("totalElements", page.getTotalElements());
        return map;
    }

    @Override
    public List<PushMallUserBillDTO> queryAll(PushMallUserBillQueryCriteria criteria) {
        return pushMallUserBillMapper.toDto(pushMallUserBillRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallUserBillDTO findById(Integer id) {
        Optional<PushMallUserBill> yxUserBill = pushMallUserBillRepository.findById(id);
        ValidationUtil.isNull(yxUserBill, "PushMallUserBill", "id", id);
        return pushMallUserBillMapper.toDto(yxUserBill.get());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallUserBillDTO create(PushMallUserBill resources) {
        return pushMallUserBillMapper.toDto(pushMallUserBillRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallUserBill resources) {
        Optional<PushMallUserBill> optionalPushMallUserBill = pushMallUserBillRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallUserBill, "PushMallUserBill", "id", resources.getId());
        PushMallUserBill pushMallUserBill = optionalPushMallUserBill.get();
        pushMallUserBill.copy(resources);
        pushMallUserBillRepository.save(pushMallUserBill);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallUserBillRepository.deleteById(id);
    }
}
