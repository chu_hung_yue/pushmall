package co.pushmall.modules.shop.service.mapper;


import co.pushmall.base.BaseMapper;
import co.pushmall.modules.shop.domain.PushMallMaterial;
import co.pushmall.modules.shop.service.dto.PushMallMaterialDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2020-01-09
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallMaterialMapper extends BaseMapper<PushMallMaterialDto, PushMallMaterial> {

}
