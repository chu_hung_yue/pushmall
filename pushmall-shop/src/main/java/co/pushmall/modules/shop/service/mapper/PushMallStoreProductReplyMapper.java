package co.pushmall.modules.shop.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.modules.shop.domain.PushMallStoreProductReply;
import co.pushmall.modules.shop.service.dto.PushMallStoreProductReplyDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-11-03
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallStoreProductReplyMapper extends EntityMapper<PushMallStoreProductReplyDTO, PushMallStoreProductReply> {

}
