package co.pushmall.modules.shop.service;

import co.pushmall.modules.shop.domain.PushMallExpress;
import co.pushmall.modules.shop.service.dto.PushMallExpressDTO;
import co.pushmall.modules.shop.service.dto.PushMallExpressQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-12-12
 */
//@CacheConfig(cacheNames = "yxExpress")
public interface PushMallExpressService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallExpressQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallExpressDTO> queryAll(PushMallExpressQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallExpressDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallExpressDTO create(PushMallExpress resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallExpress resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);
}
