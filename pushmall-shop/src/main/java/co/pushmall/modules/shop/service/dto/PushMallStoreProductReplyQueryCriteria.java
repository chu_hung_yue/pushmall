package co.pushmall.modules.shop.service.dto;

import co.pushmall.annotation.Query;
import lombok.Data;

/**
 * @author pushmall
 * @date 2019-11-03
 */
@Data
public class PushMallStoreProductReplyQueryCriteria {
    @Query
    private Integer isDel;
}
