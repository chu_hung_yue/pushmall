package co.pushmall.mp.controller;

import cn.hutool.core.date.DateUtil;
import co.pushmall.mp.domain.PushMallArticle;
import co.pushmall.mp.service.PushMallArticleService;
import co.pushmall.mp.service.dto.PushMallArticleDTO;
import co.pushmall.mp.service.dto.PushMallArticleQueryCriteria;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author pushmall
 * @date 2019-10-07
 */
@Api(tags = "商城:微信图文管理")
@RestController
@RequestMapping("api")
public class WechatArticleController {

    private final PushMallArticleService pushMallArticleService;

    public WechatArticleController(PushMallArticleService pushMallArticleService) {
        this.pushMallArticleService = pushMallArticleService;
    }

    @ApiOperation(value = "查询")
    @GetMapping(value = "/PmArticle")
    @PreAuthorize("@el.check('admin','PushMallARTICLE_ALL','PushMallARTICLE_SELECT')")
    public ResponseEntity getPushMallArticles(PushMallArticleQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity(pushMallArticleService.queryAll(criteria, pageable), HttpStatus.OK);
    }


    @ApiOperation(value = "新增")
    @PostMapping(value = "/PmArticle")
    @PreAuthorize("@el.check('admin','PushMallARTICLE_ALL','PushMallARTICLE_CREATE')")
    public ResponseEntity create(@Validated @RequestBody PushMallArticle resources) {
        resources.setAddTime(DateUtil.format(new Date(), "yyyy-MM-dd HH:mm"));
        return new ResponseEntity(pushMallArticleService.create(resources), HttpStatus.CREATED);
    }


    @ApiOperation(value = "修改")
    @PutMapping(value = "/PmArticle")
    @PreAuthorize("@el.check('admin','PushMallARTICLE_ALL','PushMallARTICLE_EDIT')")
    public ResponseEntity update(@Validated @RequestBody PushMallArticle resources) {
        pushMallArticleService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


    @ApiOperation(value = "删除")
    @DeleteMapping(value = "/PmArticle/{id}")
    @PreAuthorize("@el.check('admin','PushMallARTICLE_ALL','PushMallARTICLE_DELETE')")
    public ResponseEntity delete(@PathVariable Integer id) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        pushMallArticleService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @ApiOperation(value = "发布文章")
    @GetMapping(value = "/PmArticle/publish/{id}")
    @PreAuthorize("@el.check('admin','PushMallARTICLE_ALL','PushMallARTICLE_DELETE')")
    public ResponseEntity publish(@PathVariable Integer id) throws Exception {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        PushMallArticleDTO yxArticleDTO = pushMallArticleService.findById(id);
        pushMallArticleService.uploadNews(yxArticleDTO);
        return new ResponseEntity(HttpStatus.OK);
    }

}
