package co.pushmall.mp.repository;

import co.pushmall.mp.domain.PushMallWechatReply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-10-10
 */
public interface PushMallWechatReplyRepository extends JpaRepository<PushMallWechatReply, Integer>, JpaSpecificationExecutor {

    /**
     * findByKey
     *
     * @param key
     * @return
     */
    PushMallWechatReply findByKey(String key);
}
