package co.pushmall.mp.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.mp.domain.PushMallWechatTemplate;
import co.pushmall.mp.service.dto.PushMallWechatTemplateDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-12-10
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallWechatTemplateMapper extends EntityMapper<PushMallWechatTemplateDTO, PushMallWechatTemplate> {

}
