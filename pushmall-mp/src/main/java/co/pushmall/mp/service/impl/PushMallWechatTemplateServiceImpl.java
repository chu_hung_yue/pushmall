package co.pushmall.mp.service.impl;

import co.pushmall.mp.domain.PushMallWechatTemplate;
import co.pushmall.mp.repository.PushMallWechatTemplateRepository;
import co.pushmall.mp.service.PushMallWechatTemplateService;
import co.pushmall.mp.service.dto.PushMallWechatTemplateDTO;
import co.pushmall.mp.service.dto.PushMallWechatTemplateQueryCriteria;
import co.pushmall.mp.service.mapper.PushMallWechatTemplateMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-12-10
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallWechatTemplateServiceImpl implements PushMallWechatTemplateService {

    private final PushMallWechatTemplateRepository yxWechatTemplateRepository;

    private final PushMallWechatTemplateMapper yxWechatTemplateMapper;

    public PushMallWechatTemplateServiceImpl(PushMallWechatTemplateRepository yxWechatTemplateRepository, PushMallWechatTemplateMapper yxWechatTemplateMapper) {
        this.yxWechatTemplateRepository = yxWechatTemplateRepository;
        this.yxWechatTemplateMapper = yxWechatTemplateMapper;
    }

    @Override
    public PushMallWechatTemplate findByTempkey(String key) {
        return yxWechatTemplateRepository.findByTempkey(key);
    }

    @Override
    public Map<String, Object> queryAll(PushMallWechatTemplateQueryCriteria criteria, Pageable pageable) {
        Page<PushMallWechatTemplate> page = yxWechatTemplateRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(yxWechatTemplateMapper::toDto));
    }

    @Override
    public List<PushMallWechatTemplateDTO> queryAll(PushMallWechatTemplateQueryCriteria criteria) {
        return yxWechatTemplateMapper.toDto(yxWechatTemplateRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallWechatTemplateDTO findById(Integer id) {
        Optional<PushMallWechatTemplate> yxWechatTemplate = yxWechatTemplateRepository.findById(id);
        ValidationUtil.isNull(yxWechatTemplate, "PushMallWechatTemplate", "id", id);
        return yxWechatTemplateMapper.toDto(yxWechatTemplate.get());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallWechatTemplateDTO create(PushMallWechatTemplate resources) {
        return yxWechatTemplateMapper.toDto(yxWechatTemplateRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallWechatTemplate resources) {
        Optional<PushMallWechatTemplate> optionalPushMallWechatTemplate = yxWechatTemplateRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallWechatTemplate, "PushMallWechatTemplate", "id", resources.getId());
        PushMallWechatTemplate yxWechatTemplate = optionalPushMallWechatTemplate.get();
        yxWechatTemplate.copy(resources);
        yxWechatTemplateRepository.save(yxWechatTemplate);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        yxWechatTemplateRepository.deleteById(id);
    }
}
